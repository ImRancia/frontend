import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    
    {
      path: "/visiteurlist",
      alias: "/visiteurs",
      name: "visiteurs-list",
      component: () => import("./components/Visiteur/VisiteurList")
    },
    {
      path: "/visiteuradd",
      name: "add-visiteur",
      component: () => import("./components/Visiteur/AddVisiteur")
    },
    {
      path: "/visiteurs/:id",
      name: "visiteur-details",
      component: () => import("./components/Visiteur/Visiteur")
    },
    
    {
      path: "/sitelist",
      alias: "/sites",
      name: "sites-list",
      component: () => import("./components/site/SiteList")
    },
    {
      path: "/Siteadd",
      name: "add-site",
      component: () => import("./components/site/AddSite")
    },
    {
      path: "/sites/:id",
      name: "Site-details",
      component: () => import("./components/site/Site")
    },
    {
      path: "/visiterlist",
      alias: "/visiters",
      name: "visiters-list",
      component: () => import("./components/Visiter/VisiterList")
    },
    {
      path: "/visiteradd",
      name: "add-visiter",
      component: () => import("./components/Visiter/AddVisiter")
    },
    {
      path: "/visiters/:id",
      name: "visiter-details",
      component: () => import("./components/Visiter/Visiter")
    },
    {
      path: "/dashlist",
      alias: "/dashs",
      name: "dashs-list",
      component: () => import("./components/Dashboard/DashList")
    },
    {
      path: "/dashadd",
      name: "add-dash",
      component: () => import("./components/Dashboard/AddDash")
    },
    {
      path: "/dashs/:id",
      name: "dash-details",
      component: () => import("./components/Dashboard/Dash")
    },
    
  ]
});
