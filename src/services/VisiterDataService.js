import http from "../http-common";

class VisiterDataService {
  getAll() {
    return http.get("/visiters");
  }

  get(id) {
    return http.get(`/visiters/${id}`);
  }

  create(data) {
    return http.post("/visiters", data);
  }

  update(id, data) {
    return http.put(`/visiters/${id}`, data);
  }

  delete(id) {
    return http.delete(`/visiters/${id}`);
  }

  deleteAll() {
    return http.delete(`/visiters`);
  }

  
}

export default new VisiterDataService();
