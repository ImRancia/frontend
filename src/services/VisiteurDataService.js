import http from "../http-common";

class VisiteurDataService {
  getAll() {
    return http.get("/visiteurs");
  }

  get(id) {
    return http.get(`/visiteurs/${id}`);
  }

  create(data) {
    return http.post("/visiteurs", data);
  }

  update(id, data) {
    return http.put(`/visiteurs/${id}`, data);
  }

  delete(id) {
    return http.delete(`/visiteurs/${id}`);
  }

  deleteAll() {
    return http.delete(`/visiteurs`);
  }

  
}

export default new VisiteurDataService();
