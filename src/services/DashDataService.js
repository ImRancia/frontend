import http from "../http-common";

class DashDataService {
  getAll() {
    return http.get("/dashs");
  }

  get(id) {
    return http.get(`/dashs/${id}`);
  }

  create(data) {
    return http.post("/dashs", data);
  }

  update(id, data) {
    return http.put(`/dashs/${id}`, data);
  }

  delete(id) {
    return http.delete(`/dashs/${id}`);
  }

  deleteAll() {
    return http.delete(`/dashs`);
  }

  findByTitle(title) {
    return http.get(`/dashs?title=${title}`);
  }
}

export default new DashDataService();
